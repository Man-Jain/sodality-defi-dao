import { utils } from "ethers";
import { Contract } from "@ethersproject/contracts";
import { abis, addresses } from "@my-app/contracts";

export const getLensProxy = () => {
  const lensHubProxyInterface = new utils.Interface(abis.lensHubProxy);
  const lensHubProxyAddress = addresses.lensHubProxy;
  const contract = new Contract(lensHubProxyAddress, lensHubProxyInterface);

  return contract;
};

export const getMockProfileCreationProxy = () => {
  const mockProfileCreationProxyInterface = new utils.Interface(
    abis.mockProfileCreationProxy
  );
  const mockProfileCreationProxyAddress = addresses.mockProfileCreationProxy;
  const contract = new Contract(
    mockProfileCreationProxyAddress,
    mockProfileCreationProxyInterface
  );

  return contract;
};

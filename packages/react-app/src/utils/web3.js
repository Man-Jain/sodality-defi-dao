// import Web3 from "web3";
import Web3Modal from "web3modal";
import WalletConnectProvider from "@walletconnect/web3-provider";

const providerOptions = {
  walletconnect: {
    package: WalletConnectProvider, // required
    options: {
      infuraId: "67531e96ca3842cdabf3147f5d2a3742", // required
    },
  },
};

const web3Modal = new Web3Modal({
  providerOptions, // required
});

export const getProvider = async () => {
  const provider = await web3Modal.connect();
  // const web3 = new Web3(provider);
  return provider;
};

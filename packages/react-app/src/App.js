import { useQuery } from "@apollo/client";
import { Contract } from "@ethersproject/contracts";
import {
  shortenAddress,
  useCall,
  useEthers,
  useLookupAddress,
} from "@usedapp/core";
import React, { useEffect, useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import { Body, Button, Container, Header, Image, Link } from "./components";
import logo from "./ethereumLogo.png";

import { addresses, abis } from "@my-app/contracts";
import {
  GET_TRANSFERS,
  GET_PROFILE_BY_HANDLE,
  GET_FOLLOWING,
  GET_FOLLOWERS,
  GET_DOESFOLLOW,
  GET_FOLLOWERNFTOWNEDTOKENIDS,
  GET_PROFILE_OWNED_BY,
  GET_RECOMMENDEDPROFILES,
  GET_GLOBALPROTOCOLSTATS,
  GET_HASMIRRORED,
  PUBLICATIONS,
  PUBLICATION,
  NFT,
  VEXPLORE,
  GET_HASCOLLECTED,
} from "./graphql/subgraph";
import CreaterHome from "../src/components/CreaterDashboard/CreaterHome";
import CreaterBounties from "./components/CreaterDashboard/CreaterBounties";
import CreaterAwards from "./components/CreaterDashboard/CreaterAwards";
import CreaterProjects from "./components/CreaterDashboard/CreaterProjects";

import Drawer from "./components/Drawer";
import { getLensProxy } from "./utils/contracts";
function App() {
  // Read more about useDapp on https://usedapp.io/
  const { error: contractCallError, value: tokenBalance } =
    useCall({
      contract: new Contract(addresses.ceaErc20, abis.erc20),
      method: "balanceOf",
      args: ["0x3f8CB69d9c0ED01923F11c829BaE4D9a4CB6c82C"],
    }) ?? {};

  // const { state, send } = useContractFunction(getLensProxy(), "deposit", {
  //   transactionName: "Wrap",
  // });

  // const depositEther = (etherAmount) => {
  //   send({ value: utils.parseEther(etherAmount) });
  // };

  const { loading, error: subgraphQueryError, data } = useQuery(
    GET_PROFILE_BY_HANDLE
  );
  console.log("data: ", data);

  const { data: data1, error: error1, loading: landing1 } = useQuery(
    GET_FOLLOWERS
  );
  console.log("data1: ", data1);

  const { data: data2, error: error2, loading: landing2 } = useQuery(
    GET_DOESFOLLOW
  );
  console.log("data2: ", data2);

  const { data: data3, error: error3, loading: landing3 } = useQuery(
    GET_FOLLOWERNFTOWNEDTOKENIDS
  );
  console.log("data3: ", data3);

  const { data: data4, error: error4, loading: landing4 } = useQuery(
    GET_FOLLOWING
  );
  console.log("data4: ", data4);

  const { data: data5, error: error5, loading: landing5 } = useQuery(
    GET_PROFILE_OWNED_BY
  );
  console.log("data5: ", data5);

  const { data: data6, error: error6, loading: landing6 } = useQuery(
    GET_RECOMMENDEDPROFILES
  );
  console.log("data6: ", data6);

  const { data: data7, error: error7, loading: landing7 } = useQuery(
    GET_GLOBALPROTOCOLSTATS
  );
  console.log("data7: ", data7);

  const { data: data8, error: error8, loading: landing8 } = useQuery(
    GET_HASMIRRORED
  );
  console.log("data8: ", data8);

  const { data: data9, error: error9, loading: landing9 } = useQuery(
    GET_HASCOLLECTED
  );
  console.log("data9: ", data9);

  useEffect(() => {
    if (subgraphQueryError) {
      console.error(
        "Error while querying subgraph:",
        subgraphQueryError.message
      );
      return;
    }
    if (!loading && data && data.transfers) {
      console.log({ transfers: data.transfers });
    }
  }, [loading, subgraphQueryError, data]);

  return (
    <>
      <BrowserRouter>
        <Drawer />
        <Routes>
          <Route path="/" element={<></>} />
          <Route path="/bounty" element={<></>} />
          <Route path="/project" element={<></>} />
          <Route path="/awards" element={<></>} />
          <Route path="/tweets" element={<></>} />
          <Route path="/video" element={<></>} />
          <Route path="/new" element={<></>} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;

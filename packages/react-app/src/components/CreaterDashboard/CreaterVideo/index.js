import React, { useState } from "react";
import {
  FeedTitle,
  FeedTime,
  FeedIconButton,
  VideoTitle,
  VideoParagraph,
} from "../../index";
import Box from "@mui/material/Box";
import { useNavigate } from "react-router-dom";

import { FeedData } from "../../../utils";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Avatar from "@mui/material/Avatar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import ShareIcon from "@mui/icons-material/Share";
import PlayCircleOutlineIcon from "@mui/icons-material/PlayCircleOutline";
import Grid from "@mui/material/Grid";
import avatar from "../../../assets/avatar.png";
import { CreateFlow } from "../../Flow/createflow";

function CreaterVideo() {
  const navigate = useNavigate();

  function handleVideoClick() {
    navigate("/video");
  }
  return (
    <div>
      <Grid container spacing={1}>
        <Grid item xs={9}>
          <video width="1000" height="400" controls>
            {/* <source src=”http://techslides.com/demos/sample-videos/small.ogv” type="video/ogg" /> */}
            <source
              src="http://techslides.com/demos/sample-videos/small.ogv"
              type="video/mp4"
            />
          </video>
          <Grid style={{ marginTop: "1.5rem" }} container spacing={0}>
            <Avatar alt="Remy Sharp" src={avatar} />
            <VideoTitle>Demo Video</VideoTitle>
          </Grid>
          <VideoParagraph paragraph>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Rhoncus
            dolor purus non enim praesent elementum facilisis leo vel. Risus at
            ultrices mi tempus imperdiet. Semper risus in hendrerit gravida
            rutrum quisque non tellus. Convallis convallis tellus id interdum
            velit laoreet id donec ultrices. Odio morbi quis commodo odio aenean
            sed adipiscing. Amet nisl suscipit adipiscing bibendum est ultricies
            integer quis. Cursus euismod quis viverra nibh cras. Metus vulputate
            eu scelerisque felis imperdiet proin fermentum leo. Mauris commodo
            quis imperdiet massa tincidunt. Cras tincidunt lobortis feugiat
            vivamus at augue. At augue eget arcu dictum varius duis at
            consectetur lorem. Velit sed ullamcorper morbi tincidunt. Lorem
            donec massa sapien faucibus et molestie ac.
          </VideoParagraph>
          <VideoParagraph paragraph>
            Consequat mauris nunc congue nisi vitae suscipit. Fringilla est
            ullamcorper eget nulla facilisi etiam dignissim diam. Pulvinar
            elementum integer enim neque volutpat ac tincidunt. Ornare
            suspendisse sed nisi lacus sed viverra tellus. Purus sit amet
            volutpat consequat mauris. Elementum eu facilisis sed odio morbi.
            Euismod lacinia at quis risus sed vulputate odio. Morbi tincidunt
            ornare massa eget egestas purus viverra accumsan in. In hendrerit
            gravida rutrum quisque non tellus orci ac. Pellentesque nec nam
            aliquam sem et tortor. Habitant morbi tristique senectus et.
            Adipiscing elit duis tristique sollicitudin nibh sit. Ornare aenean
            euismod elementum nisi quis eleifend. Commodo viverra maecenas
            accumsan lacus vel facilisis. Nulla posuere sollicitudin aliquam
            ultrices sagittis orci a.
          </VideoParagraph>
          <CreateFlow />
        </Grid>
        <Grid item xs={2.8}>
          {FeedData.map((item) => (
            <Card key={item.id} style={{ marginTop: "1rem" }}>
              <FeedIconButton
                style={{ width: "22%", height: "35%" }}
                onClick={handleVideoClick}
                color="inherit"
              >
                <PlayCircleOutlineIcon fontSize="large" />
              </FeedIconButton>
              <CardMedia
                component="img"
                alt="green iguana"
                height="300"
                image={item.image}
              />
              <CardContent style={{ paddingBottom: "10px" }}>
                <Grid container spacing={0}>
                  <Grid item xs={2}>
                    <Avatar alt="Remy Sharp" src={item.avatar} />
                  </Grid>
                  <Grid item xs={7}>
                    <FeedTitle
                      style={{ margin: "0px" }}
                      gutterBottom
                      variant="h5"
                      component="div"
                    >
                      {item.title}
                    </FeedTitle>
                    <FeedTime
                      style={{ margin: "0px" }}
                      gutterBottom
                      variant="h6"
                      component="div"
                    >
                      {item.time}
                    </FeedTime>
                  </Grid>

                  <Grid item xs={3}>
                    <IconButton
                      style={{ float: "right" }}
                      onClick={handleVideoClick}
                      color="inherit"
                    >
                      <ShareIcon />
                    </IconButton>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          ))}
        </Grid>
      </Grid>
    </div>
  );
}

export default CreaterVideo;

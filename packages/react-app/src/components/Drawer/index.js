import React, { useState, useEffect } from "react";
import {
  Body,
  Button,
  Container,
  Header,
  Image,
  Link,
  MenuIconButton,
  Menu,
  MenuDrawer,
} from "../index";

import { useNavigate, useParams, useLocation } from "react-router-dom";
import { styled, useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import MuiDrawer from "@mui/material/Drawer";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Home from "@mui/icons-material/Home";
import DynamicFeed from "@mui/icons-material/DynamicFeed";
import CurrencyExchange from "@mui/icons-material/CurrencyExchange";
import Highlight from "@mui/icons-material/Highlight";
import MilitaryTech from "@mui/icons-material/MilitaryTech";
import Avatar from "@mui/material/Avatar";
import AddIcon from "@mui/icons-material/Add";
import {
  shortenAddress,
  useCall,
  useEthers,
  useLookupAddress,
} from "@usedapp/core";
import CreaterHome from "../CreaterDashboard/CreaterHome";
import CreaterAwards from "../CreaterDashboard/CreaterAwards";
import CreaterBounties from "../CreaterDashboard/CreaterBounties";
import CreaterProjects from "../CreaterDashboard/CreaterProjects";
import CreaterTweets from "../CreaterDashboard/CreaterTweets";
import CreaterVideo from "../CreaterDashboard/CreaterVideo";
import CreaterNew from "../CreaterDashboard/CreaterNew";
import avatar from "../../assets/avatar.png";
import { getProvider } from "../../utils/web3";
const drawerWidth = 240;

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

export const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));
function WalletButton() {
  const [rendered, setRendered] = useState("");

  const ens = useLookupAddress();
  const {
    account,
    activateBrowserWallet,
    deactivate,
    error,
    activate,
  } = useEthers();

  useEffect(() => {
    if (ens) {
      setRendered(ens);
    } else if (account) {
      setRendered(shortenAddress(account));
    } else {
      setRendered("");
    }
  }, [account, ens, setRendered]);

  useEffect(() => {
    if (error) {
      console.error("Error while connecting wallet:", error.message);
    }
  }, [error]);

  return (
    <Button
      onClick={async () => {
        if (!account) {
          activate(await getProvider());
        } else {
          deactivate();
        }
      }}
    >
      {rendered === "" && "Connect Wallet"}
      {rendered !== "" && rendered}
    </Button>
  );
}
function MainDrawer() {
  const navigate = useNavigate();
  console.log("navigate", navigate);
  let urlParams = useParams();
  console.log("urlParams", urlParams);
  let currentLocation = useLocation();
  console.log("currentLocation", currentLocation);
  function handleHomeClick() {
    navigate("/");
  }
  function handleBountyClick() {
    navigate("/bounty");
  }
  function handleProjectClick() {
    navigate("/project");
  }
  function handleAwardClick() {
    navigate("/awards");
  }
  function handleTweetsClick() {
    navigate("/tweets");
  }
  function handleNewClick() {
    navigate("/new");
  }
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar style={{ background: "#2f4858" }} position="fixed" open={open}>
        <Toolbar>
          <MenuIconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{
              marginRight: 5,
              ...(open && { display: "none" }),
            }}
          >
            <MenuIcon />
          </MenuIconButton>
          <Avatar alt="Remy Sharp" src={avatar} />
          <WalletButton />
        </Toolbar>
      </AppBar>
      <MenuDrawer variant="permanent" open={open}>
        <DrawerHeader>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <List>
          <ListItemButton
            onClick={handleHomeClick}
            sx={{
              minHeight: 48,
              justifyContent: open ? "initial" : "center",
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                mr: open ? 3 : "auto",
                justifyContent: "center",
              }}
            >
              <Home />
            </ListItemIcon>
            <ListItemText sx={{ opacity: open ? 1 : 0 }}>Feed</ListItemText>
          </ListItemButton>
          <ListItemButton
            onClick={handleBountyClick}
            sx={{
              minHeight: 48,
              justifyContent: open ? "initial" : "center",
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                mr: open ? 3 : "auto",
                justifyContent: "center",
              }}
            >
              <CurrencyExchange fontSize="small" />
            </ListItemIcon>
            <ListItemText sx={{ opacity: open ? 1 : 0 }}>Bounties</ListItemText>
          </ListItemButton>{" "}
          <ListItemButton
            onClick={handleProjectClick}
            sx={{
              minHeight: 48,
              justifyContent: open ? "initial" : "center",
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                mr: open ? 3 : "auto",
                justifyContent: "center",
              }}
            >
              <Highlight />
            </ListItemIcon>
            <ListItemText sx={{ opacity: open ? 1 : 0 }}>Projects</ListItemText>
          </ListItemButton>{" "}
          <ListItemButton
            onClick={handleAwardClick}
            sx={{
              minHeight: 48,
              justifyContent: open ? "initial" : "center",
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                mr: open ? 3 : "auto",
                justifyContent: "center",
              }}
            >
              <MilitaryTech />
            </ListItemIcon>
            <ListItemText sx={{ opacity: open ? 1 : 0 }}>Awards</ListItemText>
          </ListItemButton>
          <ListItemButton
            onClick={handleTweetsClick}
            sx={{
              minHeight: 48,
              justifyContent: open ? "initial" : "center",
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                mr: open ? 3 : "auto",
                justifyContent: "center",
              }}
            >
              <DynamicFeed />
            </ListItemIcon>
            <ListItemText sx={{ opacity: open ? 1 : 0 }}>Post</ListItemText>
          </ListItemButton>
          <ListItemButton
            onClick={handleNewClick}
            sx={{
              minHeight: 48,
              justifyContent: open ? "initial" : "center",
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                mr: open ? 3 : "auto",
                justifyContent: "center",
              }}
            >
              <AddIcon />
            </ListItemIcon>
            <ListItemText sx={{ opacity: open ? 1 : 0 }}>Add New</ListItemText>
          </ListItemButton>
        </List>
      </MenuDrawer>

      <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        <DrawerHeader />
        <>
          {currentLocation.pathname === "/" ? <CreaterHome /> : null}
          {currentLocation.pathname === "/project" ? <CreaterProjects /> : null}
          {currentLocation.pathname === "/awards" ? <CreaterAwards /> : null}
          {currentLocation.pathname === "/bounty" ? <CreaterBounties /> : null}
          {currentLocation.pathname === "/tweets" ? <CreaterTweets /> : null}
          {currentLocation.pathname === "/video" ? <CreaterVideo /> : null}
          {currentLocation.pathname === "/new" ? <CreaterNew /> : null}
        </>
      </Box>
    </Box>
  );
}

export default MainDrawer;

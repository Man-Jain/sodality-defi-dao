// This address points to a dummy ERC-20 contract. Replace it with your own smart contracts.
const addresses = {
  ceaErc20: "0xa6dF0C88916f3e2831A329CE46566dDfBe9E74b7",
  lensHubProxy: "0x4BF0c7AD32Fd2d32089790a54485e23f5C7736C0",
  mockProfileCreationProxy: "0x39c9Bc23B1F993B94dEC69B7Ac11C95145EC4e15",
};
export default addresses;

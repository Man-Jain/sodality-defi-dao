CivitasDAO is a DAO Governance dApp leveraging the Lens protocol to create mechanisms to allow contributors to participate to the governance On-Chain and own their contributions. 

**As a Content Creator/Influencers you can** 

- Create your own community/profile.
- Create Posts like Videos, Images and Texts 
- Get subscriptions and payment 
- Allow contributors to make proposals, discuss and vote. 
- Grow your audience and engage.
- Create bounties for your fans/followers to work on curating better content like videos, memes, funny clips, multi language subtitles and bot, spam fraud and abuse prevention.
- Earn from subscriptions and payments.
- Get crowdfunding for certain projects and initiatives. 

**As a follower you can **

- Create your profile and customise it
- Own your favourite posts as NFT and showcase them. 
- Govern and Decide on what content you would like to watch from the creator in future. ​
- Work on bounties and grow your reputation in the community while earning $$$.
- Pay in flexible ways to watch content like monthly/yearly subscriptions and/or streaming money while watching.

This is great for content creators like Musicians, Documentary Directors, Youtubers, Writers, Photographers and even Non-Profit Organisations.

## What Tech Is Used -

We are using **Lens Protocol** to create the base layer for social media which can be used to create profiles, post content, follow others and governance.

**Web3.Storage** is used for storing content on IPFS storage, Text, Music, Images and Videos all are uploaded using Web3.Storage APIs.

**OpenZeppelin** Governance Contracts are used for DAO functioning. 

**Superfluid** is Used to pay for subscriptions or get access to exclusive content, users can stream money while watching the content in real time.
